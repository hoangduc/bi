create database DDS
go

use DDS

create table LocationDesc(
	ID int identity(1,1),
	LocationName varchar(255),
	constraint PK_LocationDesc primary key (ID)
);

create table Arrest(
	ID int primary key,
	Arrest bit
);

create table Domestic(
	ID int primary key,
	Domestic bit
);

create table PrimaryType(
	ID int identity(1,1) primary key,
	PrimaryType varchar(255)
);

CREATE TABLE Crime (
	ID int,
	OccuredDate int,
	PrimaryType int,
	LocationDesc int,
	Arrest int,
	Domestic int,
	Constraint PK_Crime primary key (ID)
);


alter table Crime add constraint FK_Crime_Loca foreign key (LocationDesc) references LocationDesc(ID)
alter table Crime add constraint FK_Crime_Calendar foreign key (OccuredDate) references Calendar(DateKey)
alter table Crime add constraint FK_Crime_Arrest foreign key (Arrest) references Arrest(ID)
alter table Crime add constraint FK_Crime_Domestic foreign key (Domestic) references Domestic(ID)
alter table Crime add constraint FK_Crime_PrimaryType foreign key (PrimaryType) references PrimaryType(ID)


--==========CREATE CALENDAR===============
DECLARE @StartDate DATE = '20120101', @NumberOfYears INT = 6;

-- prevent set or regional settings from interfering with 
-- interpretation of dates / literals

SET DATEFIRST 7;
SET DATEFORMAT mdy;
SET LANGUAGE US_ENGLISH;

DECLARE @CutoffDate DATE = DATEADD(YEAR, @NumberOfYears, @StartDate);

-- this is just a holding table for intermediate calculations:

CREATE TABLE #dim
(
  [date]       DATE PRIMARY KEY, 
  [day]        AS DATEPART(DAY,      [date]),
  [month]      AS DATEPART(MONTH,    [date]),
  [MonthName]  AS DATENAME(MONTH,    [date]),
  [year]       AS DATEPART(YEAR,     [date]),
  Style112     AS CONVERT(CHAR(8),   [date], 112),
  Style101     AS CONVERT(CHAR(10),  [date], 106)
);

-- use the catalog views to generate as many rows as we need

INSERT #dim([date]) 
SELECT d
FROM
(
  SELECT d = DATEADD(DAY, rn - 1, @StartDate)
  FROM 
  (
    SELECT TOP (DATEDIFF(DAY, @StartDate, @CutoffDate)) 
      rn = ROW_NUMBER() OVER (ORDER BY s1.[object_id])
    FROM sys.all_objects AS s1
    CROSS JOIN sys.all_objects AS s2
    -- on my system this would support > 5 million days
    ORDER BY s1.[object_id]
  ) AS x
) AS y;

CREATE TABLE dbo.Calendar
(
  DateKey           INT         NOT NULL PRIMARY KEY,
  [Date]              DATE        NOT NULL,
  [Day]               TINYINT     NOT NULL,
  [Month]             TINYINT     NOT NULL,
  [MonthName]         VARCHAR(3) NOT NULL,
  [Year]              INT         NOT NULL,
);
GO

INSERT dbo.Calendar WITH (TABLOCKX)
SELECT
  DateKey     = CONVERT(INT, Style112),
  [Date]        = [date],
  [Day]         = CONVERT(TINYINT, [day]),
  [Month]       = CONVERT(TINYINT, [month]),
  [MonthName]   = CONVERT(VARCHAR(3), [MonthName]), 
  [Year]        = [year]
FROM #dim
OPTION (MAXDOP 1);


--update LocationDesc
--set LocationName=?
--where ID=?

--update Crime
--set Arrest=?, Domestic=?, PrimaryType=?, LocationDesc=?, OccuredDate=?
--where ID=?
