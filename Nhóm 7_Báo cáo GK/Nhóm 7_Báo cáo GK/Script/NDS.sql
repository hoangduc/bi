create database NDS
go

use NDS
go


CREATE TABLE IUCR (
  ID int identity(1,1) PRIMARY KEY,
  IUCRCode varchar(5),
  PrimaryDesc varchar(255),
  SecondDesc varchar(255),
  IndexCode varchar(1),
  Created datetime,
  Modified datetime,
  SourceId int,
  [Status] bit
);

CREATE TABLE Beat (
  ID int identity(1,1) PRIMARY KEY,
  BeatNum int,
  Beat int,
  TheGoem varchar(1000),
  District int,
  Sector int,
  Created datetime,
  Modified datetime,
  SourceId int
);

CREATE TABLE District (
  ID int identity(1,1) PRIMARY KEY,
  DisNum int,
  DisLabel varchar(5),
  TheGoem varchar(1000),
  Created datetime,
  Modified datetime,
  SourceId int
);

CREATE TABLE Ward (
  ID int identity(1,1) PRIMARY KEY,
  WardId int,
  TheGoem varchar(1000),
  ShapeLen float,
  ShapeArea float,
  Created datetime,
  Modified datetime,
  SourceId int
);

CREATE TABLE CommunityArea (
  ID int identity(1,1) PRIMARY KEY,
  AreaNum int,
  TheGoem varchar(1000),
  Community varchar(255),
  AreaNum1 int,
  ShapeLen float,
  ShapeArea float,
  HousingCrowed float,
  BelowPoverty float,
  Unemployed float,
  WithoutHighSchool float,
  Under18Over64 float,
  Income int,
  Hardship int,
  Created datetime,
  Modified datetime,
  SourceId1 int,
  SourceId2 int,
  [Status] bit
);

CREATE TABLE FBICode (
  ID int identity(1,1) PRIMARY KEY,
  FBICode varchar(5),
  NIBRSCode int,
  CrimeCategory varchar(255),
  Created datetime,
  Modified datetime,
  SourceId int
);

CREATE TABLE NIBRS (
  ID int identity(1,1) PRIMARY KEY,
  NIBRSCode varchar(5),
  CrimeName varchar(255),
  [Definition] varchar(255),
  Created datetime,
  Modified datetime,
  SourceId int
);


CREATE TABLE Coordinate(
	ID int identity(1,1) PRIMARY KEY,
	XCoordinate float,
	YCoordinate float,
	Latitude float,
	Longitude float,
	Created datetime,
	Modified datetime,
	SourceId int
);

CREATE TABLE Crime (
  ID int identity(1,1) PRIMARY KEY,
  [Index] int,
  CrimeID int,
  CaseNumber varchar(10),
  OccuredDate date,
  OccuredTime time,
  [Block] varchar(255),
  IUCR int,
  LocationDesc varchar(255),
  Arrest bit,
  Domestic bit,
  Beat int,
  Ward int,
  CommunityArea int,
  NIBRSCode int,
  Coordinate int,
  Created datetime,
  Modified datetime,
  SourceId int
);

ALTER TABLE Beat ADD CONSTRAINT FK_BEAT_DISTRICT FOREIGN KEY (District) REFERENCES District (ID);

ALTER TABLE FBICode ADD CONSTRAINT FK_FBICODE_NIBRSCODE FOREIGN KEY (NIBRSCode) REFERENCES NIBRS (ID);

ALTER TABLE Crime ADD CONSTRAINT FK_CRIME_IUCR FOREIGN KEY (IUCR) REFERENCES IUCR (ID);

ALTER TABLE Crime ADD CONSTRAINT FK_CRIME_BEAT FOREIGN KEY (Beat) REFERENCES Beat (ID);

ALTER TABLE Crime ADD CONSTRAINT FK_CRIME_WARD FOREIGN KEY (Ward) REFERENCES Ward (ID);

ALTER TABLE Crime ADD CONSTRAINT FK_CRIME_COMMUNITYAREA FOREIGN KEY (CommunityArea) REFERENCES CommunityArea (ID);

ALTER TABLE Crime ADD CONSTRAINT FK_CRIME_NIBRSCODE FOREIGN KEY (NIBRSCode) REFERENCES NIBRS (ID);

ALTER TABLE Crime ADD CONSTRAINT FK_CRIME_COORDINATE FOREIGN KEY (Coordinate) REFERENCES Coordinate (ID);


select distinct A.IUCR, A.PrimaryType, A.Descrition, A.SourceId
from Stage.dbo.Crime A
where A.IUCR not in (select B.IUCRCode
							from Stage.dbo.IUCR B)

select distinct LocationDesc
from Crime
where len(LocationDesc)>0
and Modified>=(select LSET from Metadata.dbo.Source where SourceId=9)
and Modified<(select CET from Metadata.dbo.Source where SourceId=9)

select distinct PrimaryDesc
from IUCR
where Modified>=(select LSET from Metadata.dbo.Source where SourceId=10)
and Modified<(select CET from Metadata.dbo.Source where SourceId=10)

select Cr.ID,
	(select A.ID from DDS.dbo.Arrest A where Cr.Arrest=A.Arrest) as Arrest_FK,
	(select D.ID from DDS.dbo.Domestic D where Cr.Domestic=D.Domestic) as Domestic_FK,
	(select Ca.DateKey from DDS.dbo.Calendar Ca where CONVERT(int, convert(char(8),Cr.OccuredDate,112)) = Ca.DateKey) as Date_FK,
	(select L.ID from DDS.dbo.LocationDesc L where L.LocationName=Cr.LocationDesc) as LocationDesc_FK,
	(select P.ID from DDS.dbo.PrimaryType P join NDS.dbo.IUCR I on I.PrimaryDesc=P.PrimaryType where Cr.IUCR=I.ID) as PrimaryType_FK
from NDS.dbo.Crime Cr
where Modified>=(select LSET from Metadata.dbo.Source where SourceId=9)
and Modified<(select CET from Metadata.dbo.Source where SourceId=9)

