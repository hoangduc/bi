create database Stage
go

use Stage
go

CREATE TABLE IUCR (
  IUCRCode varchar(5) PRIMARY KEY,
  PrimaryDesc varchar(255),
  SecondDesc varchar(255),
  IndexCode varchar(1),
  Created datetime,
  Modified datetime,
  SourceId int
);

CREATE TABLE Beat (
  BeatNum int PRIMARY KEY,
  Beat int,
  TheGoem varchar(1000),
  District int,
  Sector int,
  Created datetime,
  Modified datetime,
  SourceId int
);

CREATE TABLE District (
  DisNum int PRIMARY KEY,
  DisLabel varchar(5),
  TheGoem varchar(1000),
  Created datetime,
  Modified datetime,
  SourceId int
);

CREATE TABLE Ward (
  WardId int PRIMARY KEY,
  TheGoem varchar(1000),
  ShapeLen float,
  ShapeArea float,
  Created datetime,
  Modified datetime,
  SourceId int
);

CREATE TABLE CommunityArea1(
  AreaNum int PRIMARY KEY,
  TheGoem varchar(1000),
  Community varchar(255),
  AreaNum1 int,
  ShapeLen float,
  ShapeArea float,
  Created datetime,
  Modified datetime,
  SourceId int
);

CREATE TABLE CommunityArea2 (
  AreaNum int PRIMARY KEY,
  Community varchar(255),
  HousingCrowed float,
  BelowPoverty float,
  Unemployed float,
  WithoutHighSchool float,
  Under18Over64 float,
  Income int,
  Harship int,
  Created datetime,
  Modified datetime,
  SourceId int
);

CREATE TABLE FBICode (
  FBICode varchar(5) PRIMARY KEY,
  NIBRSCode varchar(5),
  CrimeCategory varchar(255),
  Created datetime,
  Modified datetime,
  SourceId int
);

CREATE TABLE NIBRS (
  NIBRSCode varchar(5) PRIMARY KEY,
  CrimeName varchar(255),
  [Definition] varchar(255),
  Created datetime,
  Modified datetime,
  SourceId int
);

CREATE TABLE Crime (
  [Index] int,
  CrimeID int PRIMARY KEY,
  CaseNumber varchar(10),
  [Date] datetime,
  [Block] varchar(50),
  IUCR varchar(5),
  PrimaryType varchar(255),
  Descrition varchar(255),
  LocationDesc varchar(255),
  Arrest bit,
  Domestic bit,
  Beat int,
  District int,
  Ward int,
  CommunityArea int,
  FBICode varchar(5),
  XCoordinate float,
  YCoordinate float,
  Years int,
  Latitude float,
  Longitude float,
  [Location] varchar(50),
  Created datetime,
  Modified datetime,
  SourceId int
);


select A.*, B.ID as District_FK
from Stage.dbo.Beat A join NDS.dbo.District B on A.District=B.DisNum

select A.*, B.ID as NIBRS_FK
from Stage.dbo.FBICode A join NDS.dbo.NIBRS B on A.NIBRSCode=B.NIBRSCode and A.SourceId=B.SourceId

select distinct A.XCoordinate, A.YCoordinate, A.Latitude, A.Longitude, A.Created, A.Modified, A.SourceId
from Stage.dbo.Crime A
where A.XCoordinate not in (select XCoordinate
							from NDS.dbo.Coordinate)
and A.YCoordinate not in (select YCoordinate
							from NDS.dbo.Coordinate)
and A.Latitude not in (select Latitude
							from NDS.dbo.Coordinate)
and A.Longitude not in (select Longitude
							from NDS.dbo.Coordinate)

select A.[Index], A.CrimeID, A.CaseNumber, A.[Block], A.LocationDesc, A.Arrest, A.Domestic,
CONVERT(DATE, A.[Date]) AS OccuredDate, CONVERT(TIME, A.[Date]) AS OccuredTime,
A.SourceId, A.Modified, A.Created, (select I.ID from NDS.dbo.IUCR I	where A.IUCR=I.IUCRCode) as IUCR_FK,
(select B.ID from NDS.dbo.Beat B	where B.BeatNum=A.Beat) as Beat_FK,
(select W.ID from NDS.dbo.Ward W where W.WardId = A.Ward) as Ward_FK,
(select C.ID from NDS.dbo.CommunityArea C where C.AreaNum=A.CommunityArea) as Comm_FK, 
(select N.ID from NDS.dbo.NIBRS N where N.NIBRSCode=A.FBICode) as FBI_FK,
(select Co.ID
from NDS.dbo.Coordinate Co
where Co.XCoordinate=A.XCoordinate
and Co.YCoordinate=A.YCoordinate
and Co.Latitude=A.Latitude
and Co.Longitude=A.Longitude) as Co_FK
from Stage.dbo.Crime A
where A.Modified >= (select LSET from Metadata.dbo.Source where SourceId=8)
and A.Modified < (select CET from Metadata.dbo.Source where SourceId=8)


--update NDS.dbo.Crime
--set [Index]=?, CaseNumber=?, [Block]=?, LocationDesc=?, Arrest=?, Domestic=?,
--OccuredDate=?, OccuredTime=?, Modified=?, Created=?, IUCR=?, Beat=?, Ward=?, CommunityArea=?,
--NIBRSCode=?, Coordinate=?
--where CrimeID=?
--and SourceId=?