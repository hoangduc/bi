create database Metadata
go

use Metadata
go

CREATE TABLE Source (
  SourceId int PRIMARY KEY,
  SourceName varchar(255),
  LSET datetime,
  LSEI int,
);