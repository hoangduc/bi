create database NDS
go

use NDS
go


CREATE TABLE IUCR (
  IUCRCode int PRIMARY KEY,
  PrimaryDesc varchar(255),
  SecondDesc varchar(255),
  IndexCode int,
  Created datetime,
  Modified datetime,
  SourceId int
);

CREATE TABLE Beat (
  BeatNum int PRIMARY KEY,
  Beat int,
  TheGoem varchar(255),
  District int,
  Sector int,
  Created datetime,
  Modified datetime,
  SourceId int
);

CREATE TABLE District (
  DisNum int PRIMARY KEY,
  DisLabel int,
  TheGoem varchar(255),
  Created datetime,
  Modified datetime,
  SourceId int
);

CREATE TABLE Ward (
  WardId int PRIMARY KEY,
  TheGoem varchar(255),
  ShapeLen float,
  ShapeArea float,
  Created datetime,
  Modified datetime,
  SourceId int
);

CREATE TABLE CommunityArea (
  AreaNum int PRIMARY KEY,
  TheGoem varchar(255),
  Community varchar(255),
  AreaNum1 int,
  ShapeLen float,
  ShapeArea float,
  HousingCrowed float,
  BelowPoverty float,
  Unemployed float,
  WithoutHighSchool float,
  Under18Over64 float,
  Income int,
  Harship int,
  Created datetime,
  Modified datetime,
  SourceId int
);

CREATE TABLE FBICode (
  ID int identity(1,1) PRIMARY KEY,
  FBICode varchar(255),
  NIBRSCode int,
  CrimeCategory varchar(255),
  Created datetime,
  Modified datetime,
  SourceId int
);

CREATE TABLE NIBRS (
  ID int identity(1,1) PRIMARY KEY,
  NIBRSCode varchar(255),
  CrimeName varchar(255),
  Definition varchar(255),
  Created datetime,
  Modified datetime,
  SourceId int
);

CREATE TABLE LocationDesc (
  LDID int,
  LocationDesc varchar(255),
  Created datetime,
  Modified datetime,
  SourceId int
);

CREATE TABLE Crime (
  CrimeID int PRIMARY KEY,
  CaseNumber varchar(10),
  OccuredDate date,
  OccuredTime time,
  Block varchar(255),
  IUCR int,
  LocationDesc int,
  Arrest varchar(5),
  Domestic varchar(5),
  Beat int,
  Ward int,
  CommunityArea int,
  FBICode int,
  XCoordinate float,
  YCoordinate float,
  Created datetime,
  Modified datetime,
  SourceId int
);

ALTER TABLE Beat ADD CONSTRAINT FK_BEAT_DISTRICT FOREIGN KEY (District) REFERENCES District (DisNum);

ALTER TABLE FBICode ADD CONSTRAINT FK_FBICODE_NIBRSCODE FOREIGN KEY (NIBRSCode) REFERENCES NIBRS (ID);

ALTER TABLE Crime ADD CONSTRAINT FK_CRIME_IUCR FOREIGN KEY (IUCR) REFERENCES IUCR (IUCRCode);

ALTER TABLE Crime ADD CONSTRAINT FK_CRIME_BEAT FOREIGN KEY (Beat) REFERENCES Beat (BeatNum);

ALTER TABLE Crime ADD CONSTRAINT FK_CRIME_WARD FOREIGN KEY (Ward) REFERENCES Ward (WardId);

ALTER TABLE Crime ADD CONSTRAINT FK_CRIME_COMMUNITYAREA FOREIGN KEY (CommunityArea) REFERENCES CommunityArea (AreaNum);

ALTER TABLE Crime ADD CONSTRAINT FK_CRIME_FBICODE FOREIGN KEY (FBICode) REFERENCES FBICode (ID);

ALTER TABLE Crime ADD CONSTRAINT FK_CRIME_LOCATIONDESC FOREIGN KEY (LocationDesc) REFERENCES LocationDesc (LDID);
