create database Stage
go

use Stage
go

CREATE TABLE IUCR (
  IUCRCode integer PRIMARY KEY,
  PrimaryDesc varchar(255),
  SecondDesc varchar(255),
  IndexCode int,
  Created datetime,
  Modified datetime,
  SourceId int
);

CREATE TABLE Beat (
  BeatNum int PRIMARY KEY,
  Beat int,
  TheGoem varchar(255),
  District int,
  Sector int,
  Created datetime,
  Modified datetime,
  SourceId int
);

CREATE TABLE District (
  DisNum int PRIMARY KEY,
  DisLabel int,
  TheGoem varchar(255),
  Created datetime,
  Modified datetime,
  SourceId int
);

CREATE TABLE Ward (
  WardId int PRIMARY KEY,
  TheGoem varchar(255),
  ShapeLen float,
  ShapeArea float,
  Created datetime,
  Modified datetime,
  SourceId int
);

CREATE TABLE CommunityArea (
  AreaNum int PRIMARY KEY,
  TheGoem varchar(255),
  Community varchar(255),
  AreaNum1 int,
  ShapeLen float,
  ShapeArea float,
  Created datetime,
  Modified datetime,
  SourceId int
);

CREATE TABLE CommunityArea2 (
  AreaNum int PRIMARY KEY,
  Community varchar(255),
  HousingCrowed float,
  BelowPoverty float,
  Unemployed float,
  WithoutHighSchool float,
  Under18Over64 float,
  Income int,
  Harship int,
  Created datetime,
  Modified datetime,
  SourceId int
);

CREATE TABLE FBICode (
  FBICode varchar(255) PRIMARY KEY,
  NIBRSCode varchar(255),
  CrimeCategory varchar(255),
  Created datetime,
  Modified datetime,
  SourceId int
);

CREATE TABLE NIBRS (
  NIBRSCode varchar(255) PRIMARY KEY,
  CrimeName varchar(255),
  Definition varchar(255),
  Created datetime,
  Modified datetime,
  SourceId int
);

CREATE TABLE Crime (
  ID int,
  CrimeID int PRIMARY KEY,
  CaseNumber varchar(10),
  Occured datetime,
  Block int,
  IUCR int,
  PrimaryType varchar(255),
  Descrition varchar(255),
  LocationDesc varchar(255),
  Arrest varchar(5),
  Domestic varchar(5),
  Beat int,
  District int,
  Ward int,
  CommunityArea int,
  FBICode varchar(255),
  XCoordinate float,
  YCoordinate float,
  Years int,
  Latitude float,
  Longitude float,
  Location varchar(50),
  Created datetime,
  Modified datetime,
  SourceId int
);

--ALTER TABLE Beat ADD FOREIGN KEY (District) REFERENCES District (DisNum);

--ALTER TABLE FBICode ADD FOREIGN KEY (NIBRSCode) REFERENCES NIBRS (NIBRSCode);

--ALTER TABLE Crime ADD FOREIGN KEY (IUCR) REFERENCES IUCR (IUCRCode);

--ALTER TABLE Crime ADD FOREIGN KEY (Beat) REFERENCES Beat (BeatNum);

--ALTER TABLE Crime ADD FOREIGN KEY (District) REFERENCES District (DisNum);

--ALTER TABLE Crime ADD FOREIGN KEY (Ward) REFERENCES Ward (WardId);

--ALTER TABLE Crime ADD FOREIGN KEY (CommunityArea) REFERENCES CommunityArea (AreaNum);

--ALTER TABLE Crime ADD FOREIGN KEY (FBICode) REFERENCES FBICode (FBICode);

--ALTER TABLE Crime ADD FOREIGN KEY (Block) REFERENCES Block (BlockId);
