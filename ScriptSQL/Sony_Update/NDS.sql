use [Chicago_NDS]
go

use [Chicago_NDS]
go


Create TABLE IUCR (
  ID int identity(1,1) PRIMARY KEY,
  IUCRCode varchar(5) unique,
  PrimaryDesc varchar(255),
  SecondDesc varchar(255),
  IndexCode varchar(50),
  Created datetime,
  Modified datetime,
  SourceId int
);

CREATE TABLE Beat (
  ID int identity(1,1) PRIMARY KEY,
  BeatNum int unique,
  Beat int,
  TheGoem varchar(1000),
  DistrictId int,
  Sector int,
  Created datetime,
  Modified datetime,
  SourceId int
);

create TABLE District (
  ID int identity(1,1) PRIMARY KEY,
  DisNum int unique,
  DisLabel varchar(5),
  TheGoem varchar(1000),
  Created datetime,
  Modified datetime,
  SourceId int
);

CREATE TABLE Ward (
  ID int identity(1,1) PRIMARY KEY,
  WardId int unique,
  TheGoem varchar(1000),
  ShapeLen float,
  ShapeArea float,
  Created datetime,
  Modified datetime,
  SourceId int
);

CREATE TABLE CommunityArea (
  ID int identity(1,1) PRIMARY KEY,
  AreaNum int unique,
  TheGoem varchar(1000),
  Community varchar(255),
  AreaNum1 int,
  ShapeLen float,
  ShapeArea float,
  HousingCrowed float,
  BelowPoverty float,
  Unemployed float,
  WithoutHighSchool float,
  Under18Over64 float,
  Income int,
  Harship int,
  Created datetime,
  Modified datetime,
  SourceId int
);

CREATE TABLE FBICode (
  ID int identity(1,1) PRIMARY KEY,
  FBICode varchar(5) unique,
  NIBRSCodeId int,
  CrimeCategory varchar(255),
  Created datetime,
  Modified datetime,
  SourceId int
);

CREATE TABLE NIBRS (
  ID int identity(1,1) PRIMARY KEY,
  NIBRSCode varchar(255) unique,
  CrimeName varchar(255),
  [Definition] varchar(255),
  Created datetime,
  Modified datetime,
  SourceId int
);

--CREATE TABLE LocationDesc (
--  ID int identity(1,1) PRIMARY KEY,
--  LocationDesc varchar(255),
--  Created datetime,
--  Modified datetime,
--  SourceId int
--);

CREATE TABLE Coordinate(
	ID int identity(1,1) PRIMARY KEY,
	XCoordinate float,
	YCoordinate float,
	Latitude float,
	Longitude float,
	Created datetime,
	Modified datetime,
	SourceId int
);

CREATE TABLE Crime (
  ID int identity(1,1) PRIMARY KEY,
  [Index] int,
  CrimeID int,
  CaseNumber varchar(10),
  OccuredDate date,
  OccuredTime time,
  [Block] varchar(30),
  IUCRId varchar(5),
  LocationDesc varchar(255),
  Arrest bit,
  Domestic bit,
  BeatId int,
  WardId int,
  CommunityAreaId int,
  NIBRSCodeId int,
  CoordinateId int,
  Created datetime,
  Modified datetime,
  SourceId int
);

ALTER TABLE Beat ADD CONSTRAINT FK_BEAT_DISTRICT FOREIGN KEY (District) REFERENCES District (Id)

ALTER TABLE FBICode ADD CONSTRAINT FK_FBICODE_NIBRSCODE FOREIGN KEY (NIBRSCode) REFERENCES NIBRS (Id);

ALTER TABLE Crime ADD CONSTRAINT FK_CRIME_IUCR FOREIGN KEY (IUCRId) REFERENCES IUCR (Id);

ALTER TABLE Crime ADD CONSTRAINT FK_CRIME_BEAT FOREIGN KEY (BeatId) REFERENCES Beat (Id);

ALTER TABLE Crime ADD CONSTRAINT FK_CRIME_WARD FOREIGN KEY (WardId) REFERENCES Ward (Id);

ALTER TABLE Crime ADD CONSTRAINT FK_CRIME_COMMUNITYAREA FOREIGN KEY (CommunityAreaId) REFERENCES CommunityArea (Id);

ALTER TABLE Crime ADD CONSTRAINT FK_CRIME_NIBRSCODE FOREIGN KEY (NIBRSCode) REFERENCES NIBRS (Id);

ALTER TABLE Crime ADD CONSTRAINT FK_CRIME_COORDINATE FOREIGN KEY (Coordinate) REFERENCES Coordinate (ID);

--ALTER TABLE Crime ADD CONSTRAINT FK_CRIME_LOCATIONDESC FOREIGN KEY (LocationDesc) REFERENCES LocationDesc (ID);
