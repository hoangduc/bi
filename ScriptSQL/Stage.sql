Create database ChiCago_Stage
use ChiCago_Stage


CREATE TABLE Source (
  SourceId int PRIMARY KEY not null,
  SourceName varchar(255),
  LSET datetime,
  LCEI datetime
);

CREATE TABLE IUCR (
  IUCRCode integer PRIMARY KEY,
  PrimaryDesc varchar(255),
  SecondDesc varchar(255),
  IndexCode int,
  Created datetime,
  Modified datetime,
  SourceId int
);

CREATE TABLE Beat (
  BeatNum int PRIMARY KEY
  Beat int
  TheGoem varchar(255)
  District int
  Sector int
  Created datetime
  Modified datetime
  SourceId int
);

CREATE TABLE District (
  DisNum int PRIMARY KEY
  DisLabel int
  TheGoem varchar(255)
  Created datetime
  Modified datetime
  SourceId int
);

CREATE TABLE Ward (
  WardId int PRIMARY KEY
  TheGoem varchar(255)
  ShapeLen float8
  ShapeArea float8
  Created datetime
  Modified datetime
  SourceId int
);

CREATE TABLE CommunityArea (
  AreaNum int PRIMARY KEY
  TheGoem varchar(255)
  Community varchar(255)
  AreaNum1 int
  ShapeLen float8
  ShapeArea float8
  Created datetime
  Modified datetime
  SourceId int
);

CREATE TABLE FBICode (
  FBICode int PRIMARY KEY
  NIBRSCode int
  CrimeCategory varchar(255)
  Created datetime
  Modified datetime
  SourceId int
);

CREATE TABLE NIBRS (
  NIBRSCode int PRIMARY KEY
  CrimeName varchar(255)
  Definition varchar(255)
  Created datetime
  Modified datetime
  SourceId int
);

CREATE TABLE Block (
  BlockId int
  Block varchar(255)
  Created datetime
  Modified datetime
  SourceId int
);

CREATE TABLE Crime (
  Index int
  CrimeID int PRIMARY KEY
  CaseNumber int
  Occured datetime
  Block int
  IUCR int
  PrimaryType varchar(255)
  Desc varchar(255)
  LocationDesc varchar(255)
  Arrest boolean
  Domestic boolean
  Beat int
  District int
  Ward int
  CommunityArea int
  FBICode int
  XCoordinate float8
  YCoordinate float8
  Years int
  Created datetime
  Modified datetime
  SourceId int
);

ALTER TABLE IUCR ADD FOREIGN KEY (SourceId) REFERENCES Source (SourceId);

ALTER TABLE Beat ADD FOREIGN KEY (SourceId) REFERENCES Source (SourceId);

ALTER TABLE District ADD FOREIGN KEY (SourceId) REFERENCES Source (SourceId);

ALTER TABLE Beat ADD FOREIGN KEY (District) REFERENCES District (DisNum);

ALTER TABLE Ward ADD FOREIGN KEY (SourceId) REFERENCES Source (SourceId);

ALTER TABLE CommunityArea ADD FOREIGN KEY (SourceId) REFERENCES Source (SourceId);

ALTER TABLE FBICode ADD FOREIGN KEY (SourceId) REFERENCES Source (SourceId);

ALTER TABLE FBICode ADD FOREIGN KEY (NIBRSCode) REFERENCES NIBRS (NIBRSCode);

ALTER TABLE NIBRS ADD FOREIGN KEY (SourceId) REFERENCES Source (SourceId);

ALTER TABLE Block ADD FOREIGN KEY (SourceId) REFERENCES Source (SourceId);

ALTER TABLE Crime ADD FOREIGN KEY (IUCR) REFERENCES IUCR (IUCRCode);

ALTER TABLE Crime ADD FOREIGN KEY (Beat) REFERENCES Beat (BeatNum);

ALTER TABLE Crime ADD FOREIGN KEY (District) REFERENCES District (DisNum);

ALTER TABLE Crime ADD FOREIGN KEY (Ward) REFERENCES Ward (WardId);

ALTER TABLE Crime ADD FOREIGN KEY (CommunityArea) REFERENCES CommunityArea (AreaNum);

ALTER TABLE Crime ADD FOREIGN KEY (FBICode) REFERENCES FBICode (FBICode);

ALTER TABLE Crime ADD FOREIGN KEY (SourceId) REFERENCES Source (SourceId);

ALTER TABLE Crime ADD FOREIGN KEY (Block) REFERENCES Block (BlockId);
