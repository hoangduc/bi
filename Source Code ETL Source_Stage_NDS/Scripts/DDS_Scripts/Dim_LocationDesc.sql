﻿--========================================================================================================
--Bước 1: Lấy những Địa điểm xay ra sự cố(từ bảng crime trong NDS) Đổ vào LocationDesc(bảng dim) trong DDS
--sau khi lấy xong kiểm tra xem DDS có data chưa: Nếu chưa thì đổ ngược lại thì ko cần đổ nữa.
--========================================================================================================
SELECT DISTINCT LocationDesc 
FROM Chicago_NDS.dbo.Crime
WHERE LocationDesc NOT IN (SELECT [Name] 
							FROM Chicago_DDS.dbo.LocationDesc)

--==TEST
SELECT*FROM Chicago_DDS.dbo.LocationDesc