﻿--========================================================================================================
--Bước 1: Lấy những tháng xảy ra sự cố(từ bảng crime trong NDS) Đổ vào Month(bảng dim) trong DDS
--sau khi lấy xong kiểm tra xem DDS có data chưa: Nếu chưa thì đổ ngược lại thì ko cần đổ nữa.
--========================================================================================================

SELECT DISTINCT Month(OccuredDate) AS [Month]
		, (SELECT B.ID 
			FROM Chicago_DDS.dbo.[Year] B 
			WHERE B.[year] = YEAR(OccuredDate) ) AS Year_FK
  FROM Chicago_NDS.dbo.Crime B
  WHERE NOT EXISTS (SELECT*
					FROM Chicago_DDS.dbo.[Month] 
					WHERE Year_FK =  (SELECT B.ID 
										FROM Chicago_DDS.dbo.[Year] B 
										WHERE B.[year] = YEAR(OccuredDate) ) AND [Month] = Month(OccuredDate))

--==>TEST
SELECT*FROM Chicago_DDS.dbo.Month
