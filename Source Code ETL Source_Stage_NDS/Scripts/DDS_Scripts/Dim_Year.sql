﻿--========================================================================================================
--Bước 1: Lấy những năm xảy ra sự cố(từ bảng crime trong NDS) Đổ vào Year(bảng dim) trong DDS
--========================================================================================================

SELECT DISTINCT YEAR(OccuredDate) AS [Year]
  FROM Chicago_NDS.dbo.Crime
  WHERE YEAR(OccuredDate) NOT IN (	
									SELECT A.[Year] 
									FROM Chicago_DDS.dbo.[year] A
									)

--==>TEST
SELECT*FROM Chicago_DDS.dbo.Year