﻿USE Chicago_NDS
--========================================================================================================
--Bước 1: 
-- TABLE_A: [Tần số] của vụ phạm tội của [từng năm] của từng loại tội phạm, [bị bắt hay không bị bắt] 
-- TABLE_B: [tổng tần số của các vụ phạm tội theo từng năm] (tổng = tần sô bị bắt + tần số ko bị bắt )
-- Từ [TABLE_A] và [TABLE_B] => tỉ lệ và tần số của các loại tội phạm bị bắt hay không bị bắt theo từng năm.
--========================================================================================================
CREATE VIEW View_TheftStatisticsYears
as
SELECT  (SELECT Y.Id FROM Chicago_DDS.[dbo].[Year] Y WHERE TABLE_A.Nam = Y.[Year] ) AS Year_FK
		,TABLE_A.Arrest
		,(SELECT C.Id FROM Chicago_DDS.dbo.CrimeType C WHERE C.Name LIKE TABLE_A.PrimaryDesc) AS CrimeType_FK
		,TABLE_A.Frequency
		, CAST(CAST(TABLE_A.Frequency AS DECIMAL(18,4))*100/Table_B.tong AS DECIMAL(18,2)) AS [Percent]
FROM 
	(
		SELECT YEAR(A.OccuredDate) AS Nam, A.Arrest AS Arrest,B.PrimaryDesc,COUNT(*) AS Frequency
		FROM dbo.Crime A JOIN dbo.IUCR B ON A.IUCR_FK = B.ID
		GROUP BY Year(A.OccuredDate), A.Arrest, B.PrimaryDesc  
	) AS TABLE_A
JOIN 
(
		SELECT TABLE_A.Nam, TABLE_A.PrimaryDesc, SUM(TABLE_A.Frequency) AS tong
		FROM 
		(
			SELECT YEAR(A.OccuredDate) AS Nam, A.Arrest AS Arrest,B.PrimaryDesc,COUNT(*) AS Frequency
			FROM dbo.Crime A JOIN dbo.IUCR B ON A.IUCR_FK = B.ID
			GROUP BY Year(A.OccuredDate), A.Arrest, B.PrimaryDesc  
		) AS TABLE_A
		GROUP BY TABLE_A.Nam, TABLE_A.PrimaryDesc
) as Table_B
ON TABLE_A.Nam=Table_B.Nam 
	AND TABLE_A.PrimaryDesc = Table_B.PrimaryDesc

--==>TEST
SELECT*FROM View_TheftStatisticsYears 


--========================================================================================================
--Bước 2: 
-- lấy dữ liệu từ view đổ vào DDS nếu có rồi thì không cần đổ nữa. 
--========================================================================================================
SELECT*
FROM View_TheftStatisticsYears A
WHERE NOT EXISTS (SELECT*FROM Chicago_DDS.dbo.Fact2 B
					WHERE B.Year_FK = A.Year_FK 
					AND B.isArrest = A.Arrest 
					AND B.CrimeType_FK = A.CrimeType_FK)


