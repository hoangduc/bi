﻿USE Chicago_NDS
--========================================================================================================
--Bước 1: 
-- TABLE_A: Tổng tần số phạm tội của từng địa điểm.
-- TABLE_B: Tổng tần số phạm tội là Chộm
--========================================================================================================
CREATE VIEW View_CrimeStatisticsLocation
AS
SELECT  (SELECT B.Id FROM Chicago_DDS.dbo.LocationDesc B WHERE Table_A.LocationDesc LIKE B.Name) AS LocationDesc_FK
		, CAST(CAST(TABLE_B.Theft AS DECIMAL(18,4))*100/Table_A.TongVu AS DECIMAL(18,2)) AS theft
		, (100 - CAST(CAST(TABLE_B.Theft AS DECIMAL(18,4))*100/Table_A.TongVu AS DECIMAL(18,2))) AS other
FROM 
(
	SELECT A.LocationDesc
			, COUNT(*) AS TongVu
	FROM dbo.Crime A 
	GROUP BY  A.LocationDesc
) AS Table_A
JOIN  
(
	SELECT  A.LocationDesc
			, COUNT(*) AS Theft
	FROM dbo.Crime A  
			JOIN dbo.IUCR B 
			ON A.IUCR_FK = B.ID
	WHERE B.PrimaryDesc LIKE 'THEFT'
	GROUP BY  A.LocationDesc
) AS Table_B 
  ON Table_A.LocationDesc LIKE Table_B.LocationDesc

--==>TEST
SELECT*FROM Chicago_DDS.dbo.Fact3
