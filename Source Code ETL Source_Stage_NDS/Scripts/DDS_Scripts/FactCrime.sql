
CREATE VIEW View_FactCrime
as
SELECT (SELECT A.Id 
		FROM Chicago_DDS_V2.dbo.DimMonth A JOIN Chicago_DDS_V2.dbo.DimYear B ON A.Year_FK = B.Id  
		WHERE YEAR(Table_A.OccuredDate) = B.Year AND MONTH(Table_A.OccuredDate) = A.Month ) AS Month_FK
		, (SELECT C.Id 
			FROM Chicago_DDS_v2.dbo.DimArrest C
			WHERE Table_A.Arrest = C.IsArrested
		) AS Arrest_FK
		, (SELECT D.Id
			FROM Chicago_DDS_V2.dbo.DimDomestic D
			WHERE D.IsDomestic = Table_A.Domestic) AS Domestic_FK
		, (SELECT E.Id
			FROM Chicago_DDS_V2.dbo.DimLocationDesc E
			WHERE E.Name LIKE Table_A.LocationDesc) AS LocationDesc_FK
		, (SELECT F.Id
			FROM Chicago_DDS_v2.dbo.DimCrimeType F
			WHERE TABLE_B.PrimaryDesc LIKE F.Name) AS CrimeType_FK
		, ( SELECT 
			CASE WHEN TABLE_B.PrimaryDesc LIKE '%THEFT%' THEN 1
			ELSE 0
			END	
			) AS IsTheft
FROM [Chicago_NDS].dbo.Crime Table_A JOIN Chicago_NDS.dbo.IUCR  TABLE_B ON Table_A.IUCR_FK = TABLE_B.ID

SELECT * 
FROM View_FactCrime A
WHERE NOT EXISTS (SELECT * 
					FROM Chicago_DDS_V2.dbo.FactCrime B
					WHERE B.Month_FK = A.Month_FK
						AND B.Arrest_FK = A.Arrest_FK
						AND B.Domestic_FK = A.Domestic_FK
						AND B.LocationDesc_FK = A.LocationDesc_FK
						AND B.CrimeType_FK = A.CrimeType_FK
						)

USE [Chicago_DDS_V2]
TRUNCATE TABLE [dbo].[FactCrime]