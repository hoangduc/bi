﻿USE Chicago_NDS

--========================================================================================================
--Bước 1: trong bảng crime NDS: Tính tỉ lệ [loai tội phạm] của [từng tháng] và của [từng năm] (xem test)
--TABLE_A: [Số vụ phạm] tội theo [từng tháng] của [từng năm] của từng loại tội phạm
--TABLE_B: [Tổng số vụ] theo từng năm(CT: tông = TONG(số vụ các tháng) )
-- KẾT QUẢ Bươc 1: sẽ tỉnh được [tỉ lệ] của [từng tháng] của [từng năm] của các [loại tội phạm] 
--========================================================================================================
CREATE VIEW View_CrimeStatisticsMonthOfYear_Buoc1
AS
SELECT Table_A.IUCR_FK
	,Table_A.Year
	,Table_A.Month
	,CAST(CAST(TABLE_A.SoVu AS DECIMAL(18,4))*100/Table_B.TongVuTheoNamCuaTungIUCR AS DECIMAL(18,2)) AS [Percent]
FROM 
(
	SELECT A.IUCR_FK ,YEAR(A.OccuredDate) AS Year, MONTH(A.OccuredDate) AS Month , COUNT(*) AS SoVu
	FROM Chicago_NDS.dbo.Crime A
	WHERE A.IUCR_FK IS NOT null 
	GROUP BY A.IUCR_FK ,YEAR(A.OccuredDate) , MONTH(A.OccuredDate)
	--ORDER BY A.IUCR_FK ASC, Year ASC, Month ASC
) AS Table_A
JOIN 
(
	SELECT A.IUCR_FK ,YEAR(A.OccuredDate) AS Year, COUNT(*) AS TongVuTheoNamCuaTungIUCR
	FROM Chicago_NDS.dbo.Crime A
	WHERE A.IUCR_FK IS NOT null
	GROUP BY A.IUCR_FK, YEAR(A.OccuredDate)
	--ORDER BY A.IUCR_FK,Year
) AS TABLE_B ON table_A.IUCR_FK = TABLE_B.IUCR_FK AND Table_A.Year = TABLE_B.Year

--==>TEST: 
SELECT*FROM View_CrimeStatisticsMonthOfYear_Buoc1
ORDER BY IUCR_FK ASC, [Year] ASC , [Month] ASC 
SELECT*FROM Chicago_DDS.dbo.Month

--========================================================================================================
--Bước 2: Vì trong bảng CrimeType trong DDS đang lưu theo (ID, Name) nên ta phải lấy được tên của nó
	-- Từ Year,Month ta phải lấy được ID trong dim từ bảng Month trong DDS
--========================================================================================================
CREATE VIEW View_CrimeStatisticsMonthOfYear_Buoc2
AS 
SELECT  (SELECT C.Id FROM Chicago_DDS.dbo.CrimeType C 
					 WHERE C.Name LIKE B.PrimaryDesc) AS CrimeType_FK
		, (SELECT ID FROM Chicago_DDS.dbo.[Month] M 
					 WHERE M.Month = A.Month AND M.Year_FK = (SELECT Y.Id
														FROM Chicago_DDS.dbo.Year Y 
													WHERE A.year = Y.Year) ) AS Month_FK
		, A.[Year]
		, A.[Percent]
FROM View_CrimeStatisticsMonthOfYear_Buoc1 A JOIN dbo.IUCR B
	ON B.ID = A.IUCR_FK

--==>TEST
SELECT*FROM View_CrimeStatisticsMonthOfYear_Buoc2
ORDER BY CrimeType_FK ASC 
-- Kết quả sẽ hiển thị ở mức chi tiết của loại tội phạm (Giết người: giết người cướp của Trên 500nghin, hay Giết người cướp của <500 ngìn)
-- Vì vậy ta cần phải group by thêm 1 lần nữa xuống bước 3


--========================================================================================================
--Bước 3: 
-- TABLE_A: [tỉ lệ phạm tội] của [từng tháng] theo [từng năm] của ơtừng loại tội phạm] => Giải quyết vấn đề bước 2
-- TABLE_B: [Tổng tỉ lệ phạm tội của từng năm] của [từng loại tọi phạm]
--========================================================================================================

CREATE VIEW View_CrimeStatisticsMonthOfYear
as
SELECT Table_A.CrimeType_FK
		, Table_A.Month_FK
		, CAST(CAST(TABLE_A.[Percent] AS DECIMAL(18,4))*100/Table_B.TongPercent AS DECIMAL(18,2)) AS [Percent]
FROM 
(
	SELECT A.CrimeType_FK, A.Year, A.Month_FK, Sum(A.[Percent]) AS [Percent]
	FROM View_CrimeStatisticsMonthOfYear_Buoc2 A
	GROUP BY A.CrimeType_FK,A.Year, A.Month_FK
	ORDER BY A.CrimeType_FK, A.Year,A.Month_FK
) AS Table_A
JOIN 
(
	SELECT B.CrimeType_FK, B.Year, SUM(B.[Percent]) AS TongPercent
	FROM View_CrimeStatisticsMonthOfYear_Buoc2 B
	GROUP BY B.CrimeType_FK, B.Year
) AS Table_B ON Table_A.CrimeType_FK = Table_B.CrimeType_FK AND Table_A.year = Table_B.year


--==>TEST

SELECT*FROM View_CrimeStatisticsMonthOfYear
ORDER BY CrimeType_FK, Month_FK

--========================================================================================================
--Bước 4: Những data nào trong DDS có rồi thì không cần lấy nữa! 
--========================================================================================================

SELECT*
FROM View_CrimeStatisticsMonthOfYear A
WHERE NOT EXISTS (
				  SELECT*
				  FROM Chicago_DDS.dbo.Fact1_Month B
				  WHERE A.CrimeType_FK = B.CrimeType_FK
						AND A.Month_FK = B.Month_FK
						AND A.[Percent] = B.[Percent]
				  )
ORDER BY CrimeType_FK, Month_FK

--===>TEST
SELECT*FROM Chicago_DDS.dbo.Fact1_Month