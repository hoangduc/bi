﻿USE Chicago_NDS
--==============================================================================================
--Bước1:
	-- TABLE_A: [Tần số phạm tôi] các [loại tôi phạm] theo từng [năm]
	-- TABLE_B:  [Tần số phạm tội] của [Loại tội phạm] qua tất các năm

--===========================================================================================

CREATE VIEW view_CrimeStatisticsYear_Buoc1
as
SELECT TABLE_A.Year
		, (SELECT B.PrimaryDesc FROM Chicago_NDS.dbo.IUCR B WHERE TABLE_A.IUCR_FK = B.ID) AS IUCR
		, CAST(CAST(TABLE_A.SoVu AS DECIMAL(18,4))*100/Table_B.Tong AS DECIMAL(18,2)) AS [Percent]
FROM 
(
	SELECT  YEAR(A.OccuredDate) AS [Year], A.IUCR_FK ,  COUNT(*) AS SoVu
	FROM dbo.Crime A
	WHERE A.IUCR_FK IS NOT null
	GROUP BY   YEAR(A.OccuredDate), A.IUCR_FK
	--ORDER BY A.IUCR_FK ASC , YEAR(A.OccuredDate) ASC 
) AS TABLE_A
JOIN 
(
	SELECT A.IUCR_FK, COUNT(*) AS Tong
	FROM dbo.Crime A
	WHERE A.IUCR_FK IS NOT null
	GROUP BY A.IUCR_FK
	--ORDER BY A.IUCR_FK
) AS TABLE_B
ON TABLE_A.IUCR_FK = TABLE_B.IUCR_FK

--==>TEST
SELECT*FROM view_CrimeStatisticsYear_Buoc1
ORDER BY IUCR ASC, Year ASC
--==>Thể hiện mức chi tiết: VD: cũng là loại giết người nhưng có giết người do cố ý, Giết ngươi vô ý!
--==>Do đó ta cần tính củ thể (Bước3)


---=================================================================================
--====Bước 2: 
-- Chỉ đơn giản chỉ đưa về dạng _FK củ thể để đổ vào  
--==================================================================================

CREATE VIEW view_CrimeStatisticsYear_main_Buoc2
as
SELECT (SELECT B.Id FROM Chicago_DDS.dbo.Year B WHERE B.Year = A.year) AS Year_FK
		,(SELECT B.Id FROM Chicago_DDS.dbo.CrimeType B WHERE B.Name LIKE A.IUCR) AS CrimeType_FK
		,A.[Percent]
FROM view_CrimeStatisticsYear_Buoc1 A

-->TEST 
SELECT*FROM view_CrimeStatisticsYear_main_Buoc2
ORDER BY CrimeType_FK ASC, Year_FK ASC

---=================================================================================
--====Bước 3: 
--TABLE_A: Tổng tỉ lệ của loại phạm tội theo từng năm
--TABLE_B: tổng tỉ lệ của loại tội phạm theo tất cả các năm
-- Đưa về tần số cụ thể theo công thức VD: Tỉ lệ cần tính = Tỉ lệ (TABLE_A)/ tỉ lệ(TABLE_B)
--==================================================================================
CREATE VIEW view_CrimeStatisticsYear
AS 
SELECT TABLE_A.CrimeType_FK
		, TABLE_A.Year_FK
		, CAST(CAST(TABLE_A.[Percent] AS DECIMAL(18,4))*100/Table_B.Tong_Percent AS DECIMAL(18,2)) AS [Percent]
FROM 
(
	SELECT CrimeType_FK, Year_FK, SUM([Percent]) AS [Percent]
	FROM view_CrimeStatisticsYear_main_Buoc2
	GROUP BY CrimeType_FK, Year_FK
	ORDER BY CrimeType_FK ASC, Year_FK ASC
) AS TABLE_A
JOIN 
(
	SELECT CrimeType_FK, SUM([Percent]) AS Tong_Percent
	FROM view_CrimeStatisticsYear_main_Buoc2
	GROUP BY CrimeType_FK
	--ORDER BY CrimeType_FK
) AS TABLE_B ON TABLE_A.CrimeType_FK = TABLE_B.CrimeType_FK

--==>TEST
SELECT*FROM view_CrimeStatisticsYear
ORDER BY CrimeType_FK ASC, Year_FK ASC
---=================================================================================
--====Bước 4: đổ vào DDS và những dữ liệu nào trong DDS có rồi thì không lấy vào nữa!
--==================================================================================
SELECT*
FROM view_CrimeStatisticsYear A
WHERE NOT EXISTS(SELECT*
			 	 FROM Chicago_DDS.dbo.Fact1_Year B 
				 WHERE A.CrimeType_FK = B.CrimeType_FK
						AND A.Year_FK = B.Year_FK
						AND A.[Percent] = B.[Percent]
				)
ORDER BY A.CrimeType_FK ASC, A.Year_FK ASC


--==>Kết quả
SELECT*FROM Chicago_DDS.dbo.Fact1_Year
