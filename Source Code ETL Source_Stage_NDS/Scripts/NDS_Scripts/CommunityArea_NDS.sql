﻿--========================================================================================================

-- CommnunityArea 1
SELECT AreaNum
		, TheGoem
		, Community
		, AreaNum1
		, ShapeLen
		, ShapeArea
  FROM Chicago_Stage.dbo.CommunityArea

--Update
UPDATE Table_A
SET Table_A.TheGoem = ?
	,Table_A.Community = ?
	,Table_A.AreaNum1 = ?
	,Table_A.ShapeLen = ?
	,Table_A.ShapeArea = ?
	,Table_A.Modified = ?
FROM Chicago_NDS.dbo.CommunityArea Table_A
WHERE Table_A.AreaNum = ? AND Table_A.SourceId = ?
--========================================================================================================
---comnutityArea 2
  SELECT	Table_A.AreaNum
			,Table_A.HousingCrowed
			,Table_A.BelowPoverty
			,Table_A.Unemployed
			,Table_A.WithoutHighSchool
			,Table_A.Under18Over64
			,Table_A.Income
			,Table_A.Harship
  FROM dbo.CommunityArea2 Table_A
-- Lookup
SELECT AreaNum 
FROM Chicago_NDS.dbo.CommunityArea
--Update
UPDATE Table_A
SET Table_A.HousingCrowed = ?
	, Table_A.BelowPoverty = ?
	, Table_A.Unemployed = ?
	, Table_A.WithoutHighSchool = ?
	, Table_A.Under18Over64 = ?
	, Table_A.Income = ?
	, Table_A.Harship = ?
	, Table_A.Modified = ?
FROM Chicago_NDS.dbo.CommunityArea Table_A
WHERE Table_A.SourceID2 = ? AND Table_A.AreaNum = ?
--========================================================================================================
USE Chicago_Stage
CREATE PROCEDURE str_DELETE_CommunityArea
as
DELETE Table_A
FROM Chicago_NDS.dbo.CommunityArea Table_A
WHERE Table_A.AreaNum NOT IN (SELECT A.AreaNum 
								FROM Chicago_Stage.dbo.CommunityArea A 
								JOIN Chicago_Stage.dbo.CommunityArea2 B 
								ON B.AreaNum = A.AreaNum )

EXEC str_DELETE_CommunityArea