﻿
--========================================================================================================
--Câu lệnh lấy dữ liệu:
--Chú ý câu lệnh này sẽ làm chậm !!!
--========================================================================================================
SELECT DISTINCT Table_A.XCoordinate
		, Table_A.YCoordinate
		, Table_A.Latitude
		, Table_A.Longitude
FROM Chicago_Stage.dbo.Crime Table_A
WHERE NOT EXISTS (SELECT* 
					FROM Chicago_NDS.dbo.Coordinate Table_B 
					WHERE Table_A.XCoordinate = Table_B.XCoordinate
						AND Table_A.YCoordinate = Table_B.YCoordinate
						AND Table_A.Latitude = Table_B.Latitude
						AND Table_A.Longitude = Table_B.Longitude)

--==== Câu lệnh Insert
INSERT INTO Chicago_NDS.dbo.Coordinate
(
    XCoordinate,
    YCoordinate,
    Latitude,
    Longitude,
    Created,
    Modified,
    SourceId
)
VALUES (?,?,?,?,?,?,?)

