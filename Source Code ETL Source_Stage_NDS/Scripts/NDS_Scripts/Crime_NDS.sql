--========================================================================================================
-- TABLE CRIME IN NDS
--========================================================================================================
SELECT A.ID AS [Index], A.CrimeID
		, A.CaseNumber, CONVERT(DATE
		, A.Occured) AS OccuredDate
		, CONVERT(TIME, A.Occured) AS OccuredTime
		, A.Block 
		, (SELECT B.ID FROM Chicago_NDS.dbo.IUCR B WHERE A.IUCR LIKE B.IUCRCode AND B.SourceId = 8) AS IUCR_FK
		, A.LocationDesc, A.Arrest, A.Domestic
		, (SELECT beat.ID FROM Chicago_NDS.dbo.Beat beat WHERE A.Beat = beat.BeatNum) AS Beat_FK
		, (SELECT ward.ID FROM Chicago_NDS.dbo.Ward ward WHERE A.Ward = ward.WardId) AS ward_FK
		, (SELECT com.ID FROM Chicago_NDS.dbo.CommunityArea com WHERE A.CommunityArea = com.AreaNum ) AS CommunityArea_FK
		, (SELECT ni.ID FROM Chicago_NDS.dbo.NIBRS ni WHERE A.FBICode LIKE ni.NIBRSCode ) AS NIBRSCode_FK
		, (SELECT coor.ID FROM Chicago_NDS.dbo.Coordinate coor WHERE A.XCoordinate = coor.XCoordinate AND coor.YCoordinate = A.YCoordinate AND coor.Latitude = A.Latitude AND coor.Longitude = A.Longitude) AS Coordinate_FK
FROM Chicago_Stage.dbo.Crime A



