--========================================================================================================
--FBICode trong NDS
--========================================================================================================

SELECT A.FBICode
		,(select B.ID from Chicago_NDS.dbo.NIBRS B where A.NIBRSCode like B.NIBRSCode) as NIBRSCode_FK
		, A.CrimeCategory
FROM Chicago_Stage.dbo.FBICode A

-- LOOKUP
SELECT A.FBICode 
FROM Chicago_NDS.dbo.FBICode A
WHERE A.SourceId = 5


--Update
UPDATE A
SET A.NIBRSCode_FK = ?,
	A.CrimeCategory = ?,
	A.Modified = ?
FROM Chicago_NDS.dbo.FBICode A WHERE A.SourceId = ? AND A.FBICode LIKE ?