﻿--========================================================================================================
--IUCR trong NDS
-- Tại LOOUP có phải có mã và id source: bởi vì 
-- Giả sử nếu không có Id source thì khi bảng 2 chạy nếu có ID giống nhau nó sẽ vào update như vậy là không đúng
--========================================================================================================

-- ==============
-- ==   IUCR   ==
-- ==============
USE Chicago_Stage
--== SELECT lấy trong bản IUCR
SELECT Table_A.IUCRCode
		, Table_A.PrimaryDesc
		, Table_A.SecondDesc
		, Table_A.IndexCode
FROM Chicago_Stage.dbo.IUCR Table_A

--== LOOKUP: 
SELECT IUCRCode, SourceId
FROM Chicago_NDS.dbo.IUCR 
WHERE SourceId = 6

-- Update
UPDATE Table_A 
SET Table_A.PrimaryDesc = ?
	, Table_A.SecondDesc = ?
	, Table_A.IndexCode = ?
	, Table_A.Modified = ?
FROM Chicago_NDS.dbo.IUCR Table_A WHERE Table_A.SourceId = ? AND Table_A.IUCRCode = ? 

--========================================================================================================
--========================================================================================================
-- ==============
-- ==   CRIME  ==
-- ==============

--== SELECT lấy trong bản IUCR
--CREATE VIEW IUCR_CRIME
--as
SELECT Table_B.IUCRCode
		, Table_B.PrimaryType
		, Table_B.Descrition
FROM 
(
SELECT  Table_A.IUCR AS IUCRCode
		, Table_A.PrimaryType
		, Table_A.Descrition
		, ROW_NUMBER() OVER (PARTITION BY Table_A.IUCR ORDER BY Table_A.IUCR DESC) AS RowNumber 
FROM Chicago_Stage.dbo.Crime Table_A) AS Table_B
WHERE RowNumber = 1


--LOOKUP
SELECT IUCRCode, SourceId
FROM Chicago_NDS.dbo.IUCR WHERE SourceId = 8

--UDDATE
UPDATE Table_A
SET Table_A.PrimaryDesc = ?
	, Table_A.SecondDesc = ?
	, Table_A.Modified = ?
FROM Chicago_NDS.dbo.IUCR Table_A 
WHERE Table_A.SourceId = ? AND Table_A.IUCRCode = ?

-- ==========CÂU LỆNH DELETE=========
CREATE PROCEDURE DELETE_IUCR
as 
DELETE Table_A
FROM Chicago_NDS.dbo.IUCR Table_A
WHERE Table_A.IUCRCode NOT IN (SELECT IUCRCode 
								FROM Chicago_Stage.dbo.IUCR)
	AND Table_A.SourceId IN (SELECT SourceId
								FROM Chicago_Stage.dbo.IUCR)

DELETE Table_B
FROM Chicago_NDS.dbo.IUCR Table_B
WHERE Table_B.IUCRCode NOT IN (SELECT IUCRCode
								FROM IUCR_CRIME)  
	AND Table_B.SourceId IN (SELECT SourceId
								FROM Chicago_Stage.dbo.Crime)

-- thực thi							
EXEC DELETE_IUCR
--TEST
SELECT*FROM Chicago_NDS.dbo.IUCR

