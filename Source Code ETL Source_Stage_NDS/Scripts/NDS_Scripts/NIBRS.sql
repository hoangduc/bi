﻿--========================================================================================================
-- TABLE NIBRS trong Stage vào NDS
--========================================================================================================
USE Chicago_Stage
-- Câu lệnh select 
select A.NIBRSCode
		, A.CrimeName
		, A.Definition 
FROM Chicago_Stage.dbo.NIBRS A

-- câu lệnh update
update A
set A.CrimeName = ?,
	A.Definition = ?,
	A.Modified = ?
from Chicago_NDS.dbo.NIBRS A 
where A.SourceId = ? and A.NIBRSCode = ?

-- Câu lệnh delete 
DELETE Chicago_NDS.dbo.NIBRS 
WHERE NIBRSCode NOT IN  (SELECT NIBRSCode FROM Chicago_Stage.dbo.NIBRS) 
	AND SourceId IN (SELECT SourceId from Chicago_Stage.dbo.NIBRS)