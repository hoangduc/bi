CREATE VIEW ViewMiningCrime
AS 
SELECT A.ID
		, A.LocationDesc 
		, B.SecondDesc
		, C.AreaNum
		, C.Community
		, C.HousingCrowed
		, C.BelowPoverty
		, C.Unemployed
		, C.WithoutHighSchool
		, C.Harship
		, C.Under18Over64
		, (SELECT CASE WHEN B.PrimaryDesc LIKE '%THEFT%' then 'Y'
			ELSE 'N' END ) AS isTheft
FROM Chicago_NDS.dbo.Crime A JOIN Chicago_NDS.dbo.IUCR B ON A.IUCR_FK = B.ID
							JOIN Chicago_NDS.dbo.CommunityArea C ON A.CommunityArea_FK = C.ID 

USE [Chicago_NDS]
ALTER TABLE [dbo].[Ward]
ADD Active  bit 

							
							