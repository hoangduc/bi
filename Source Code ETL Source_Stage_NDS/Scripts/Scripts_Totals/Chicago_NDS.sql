USE [Chicago_NDS]
GO
/****** Object:  Table [dbo].[Crime]    Script Date: 30/10/2019 9:26:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Crime](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Index] [int] NULL,
	[CrimeID] [int] NULL,
	[CaseNumber] [varchar](10) NULL,
	[OccuredDate] [date] NULL,
	[OccuredTime] [time](7) NULL,
	[Block] [varchar](50) NULL,
	[IUCR_FK] [int] NULL,
	[LocationDesc] [varchar](255) NULL,
	[Arrest] [bit] NULL,
	[Domestic] [bit] NULL,
	[Beat_FK] [int] NULL,
	[Ward_FK] [int] NULL,
	[CommunityArea_FK] [int] NULL,
	[NIBRSCode_FK] [int] NULL,
	[Coordinate_FK] [int] NULL,
	[Created] [datetime] NULL,
	[Modified] [datetime] NULL,
	[SourceId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UC_CrimeID] UNIQUE NONCLUSTERED 
(
	[CrimeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[View_CrimeStatisticsMonthOfYear_Buoc1]    Script Date: 30/10/2019 9:26:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_CrimeStatisticsMonthOfYear_Buoc1]
AS
SELECT Table_A.IUCR_FK
	,Table_A.Year
	,Table_A.Month
	,CAST(CAST(TABLE_A.SoVu AS DECIMAL(18,4))*100/Table_B.TongVuTheoNamCuaTungIUCR AS DECIMAL(18,2)) AS [Percent]
FROM 
(
	SELECT A.IUCR_FK ,YEAR(A.OccuredDate) AS Year, MONTH(A.OccuredDate) AS Month , COUNT(*) AS SoVu
	FROM Chicago_NDS.dbo.Crime A
	WHERE A.IUCR_FK IS NOT null 
	GROUP BY A.IUCR_FK ,YEAR(A.OccuredDate) , MONTH(A.OccuredDate)
) AS Table_A
JOIN 
(
	SELECT A.IUCR_FK ,YEAR(A.OccuredDate) AS Year, COUNT(*) AS TongVuTheoNamCuaTungIUCR
	FROM Chicago_NDS.dbo.Crime A
	WHERE A.IUCR_FK IS NOT null
	GROUP BY A.IUCR_FK, YEAR(A.OccuredDate)
) AS TABLE_B ON table_A.IUCR_FK = TABLE_B.IUCR_FK AND Table_A.Year = TABLE_B.Year
GO
/****** Object:  Table [dbo].[IUCR]    Script Date: 30/10/2019 9:26:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IUCR](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IUCRCode] [varchar](5) NULL,
	[PrimaryDesc] [varchar](255) NULL,
	[SecondDesc] [varchar](255) NULL,
	[IndexCode] [varchar](50) NULL,
	[Created] [datetime] NULL,
	[Modified] [datetime] NULL,
	[SourceId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[View_CrimeStatisticsMonthOfYear_Buoc2]    Script Date: 30/10/2019 9:26:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_CrimeStatisticsMonthOfYear_Buoc2]
AS 
SELECT  (SELECT C.Id FROM Chicago_DDS.dbo.CrimeType C 
					 WHERE C.Name LIKE B.PrimaryDesc) AS CrimeType_FK
		, (SELECT ID FROM Chicago_DDS.dbo.[Month] M 
					 WHERE M.Month = A.Month AND M.Year_FK = (SELECT Y.Id
														FROM Chicago_DDS.dbo.Year Y 
													WHERE A.year = Y.Year) ) AS Month_FK
		, A.[Year]
		, A.[Percent]
FROM View_CrimeStatisticsMonthOfYear_Buoc1 A JOIN dbo.IUCR B
	ON B.ID = A.IUCR_FK
GO
/****** Object:  View [dbo].[View_CrimeStatisticsMonthOfYear]    Script Date: 30/10/2019 9:26:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_CrimeStatisticsMonthOfYear]
as
SELECT Table_A.CrimeType_FK
		, Table_A.Month_FK
		, CAST(CAST(TABLE_A.[Percent] AS DECIMAL(18,4))*100/Table_B.TongPercent AS DECIMAL(18,2)) AS [Percent]
FROM 
(
	SELECT A.CrimeType_FK, A.Year, A.Month_FK, Sum(A.[Percent]) AS [Percent]
	FROM View_CrimeStatisticsMonthOfYear_Buoc2 A
	GROUP BY A.CrimeType_FK,A.Year, A.Month_FK
) AS Table_A
JOIN 
(
	SELECT B.CrimeType_FK, B.Year, SUM(B.[Percent]) AS TongPercent
	FROM View_CrimeStatisticsMonthOfYear_Buoc2 B
	GROUP BY B.CrimeType_FK, B.Year
) AS Table_B ON Table_A.CrimeType_FK = Table_B.CrimeType_FK AND Table_A.year = Table_B.year
GO
/****** Object:  View [dbo].[View_TheftStatisticsYears]    Script Date: 30/10/2019 9:26:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[View_TheftStatisticsYears]
as
SELECT  (SELECT Y.Id FROM Chicago_DDS.[dbo].[Year] Y WHERE TABLE_A.Nam = Y.[Year] ) AS Year_FK
		,TABLE_A.Arrest
		,(SELECT C.Id FROM Chicago_DDS.dbo.CrimeType C WHERE C.Name LIKE TABLE_A.PrimaryDesc) AS CrimeType_FK
		,TABLE_A.Frequency
		, CAST(CAST(TABLE_A.Frequency AS DECIMAL(18,4))*100/Table_B.tong AS DECIMAL(18,2)) AS [Percent]
FROM 
	(
		SELECT YEAR(A.OccuredDate) AS Nam, A.Arrest AS Arrest,B.PrimaryDesc,COUNT(*) AS Frequency
		FROM dbo.Crime A JOIN dbo.IUCR B ON A.IUCR_FK = B.ID
		GROUP BY Year(A.OccuredDate), A.Arrest, B.PrimaryDesc  
	) AS TABLE_A
JOIN 
(
		SELECT TABLE_A.Nam, TABLE_A.PrimaryDesc, SUM(TABLE_A.Frequency) AS tong
		FROM 
		(
			SELECT YEAR(A.OccuredDate) AS Nam, A.Arrest AS Arrest,B.PrimaryDesc,COUNT(*) AS Frequency
			FROM dbo.Crime A JOIN dbo.IUCR B ON A.IUCR_FK = B.ID
			GROUP BY Year(A.OccuredDate), A.Arrest, B.PrimaryDesc  
		) AS TABLE_A
		GROUP BY TABLE_A.Nam, TABLE_A.PrimaryDesc
) as Table_B
ON TABLE_A.Nam=Table_B.Nam 
	AND TABLE_A.PrimaryDesc = Table_B.PrimaryDesc
GO
/****** Object:  View [dbo].[View_CrimeStatisticsLocation]    Script Date: 30/10/2019 9:26:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_CrimeStatisticsLocation]
AS
SELECT  (SELECT B.Id FROM Chicago_DDS.dbo.LocationDesc B WHERE Table_A.LocationDesc LIKE B.Name) AS LocationDesc_FK
		, CAST(CAST(TABLE_B.Theft AS DECIMAL(18,4))*100/Table_A.TongVu AS DECIMAL(18,2)) AS theft
		, (100 - CAST(CAST(TABLE_B.Theft AS DECIMAL(18,4))*100/Table_A.TongVu AS DECIMAL(18,2))) AS other
FROM 
(
	SELECT A.LocationDesc
			, COUNT(*) AS TongVu
	FROM dbo.Crime A 
	GROUP BY  A.LocationDesc
) AS Table_A
JOIN  
(
	SELECT  A.LocationDesc
			, COUNT(*) AS Theft
	FROM dbo.Crime A  
			JOIN dbo.IUCR B 
			ON A.IUCR_FK = B.ID
	WHERE B.PrimaryDesc LIKE 'THEFT'
	GROUP BY  A.LocationDesc
) AS Table_B 
  ON Table_A.LocationDesc LIKE Table_B.LocationDesc
GO
/****** Object:  View [dbo].[view_CrimeStatisticsYear_Buoc1]    Script Date: 30/10/2019 9:26:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_CrimeStatisticsYear_Buoc1]
as
SELECT TABLE_A.Year
		, (SELECT B.PrimaryDesc FROM Chicago_NDS.dbo.IUCR B WHERE TABLE_A.IUCR_FK = B.ID) AS IUCR
		, CAST(CAST(TABLE_A.SoVu AS DECIMAL(18,4))*100/Table_B.Tong AS DECIMAL(18,2)) AS [Percent]
FROM 
(
	SELECT  YEAR(A.OccuredDate) AS [Year], A.IUCR_FK ,  COUNT(*) AS SoVu
	FROM dbo.Crime A
	WHERE A.IUCR_FK IS NOT null
	GROUP BY   YEAR(A.OccuredDate), A.IUCR_FK
) AS TABLE_A
JOIN 
(
	SELECT A.IUCR_FK, COUNT(*) AS Tong
	FROM dbo.Crime A
	WHERE A.IUCR_FK IS NOT null
	GROUP BY A.IUCR_FK
) AS TABLE_B
ON TABLE_A.IUCR_FK = TABLE_B.IUCR_FK
GO
/****** Object:  View [dbo].[view_CrimeStatisticsYear_main_Buoc2]    Script Date: 30/10/2019 9:26:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_CrimeStatisticsYear_main_Buoc2]
as
SELECT (SELECT B.Id FROM Chicago_DDS.dbo.Year B WHERE B.Year = A.year) AS Year_FK
		,(SELECT B.Id FROM Chicago_DDS.dbo.CrimeType B WHERE B.Name LIKE A.IUCR) AS CrimeType_FK
		,A.[Percent]
FROM view_CrimeStatisticsYear_Buoc1 A
GO
/****** Object:  View [dbo].[view_CrimeStatisticsYear]    Script Date: 30/10/2019 9:26:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_CrimeStatisticsYear]
AS 
SELECT TABLE_A.CrimeType_FK
		, TABLE_A.Year_FK
		, CAST(CAST(TABLE_A.[Percent] AS DECIMAL(18,4))*100/Table_B.Tong_Percent AS DECIMAL(18,2)) AS [Percent]
FROM 
(
	SELECT CrimeType_FK, Year_FK, SUM([Percent]) AS [Percent]
	FROM view_CrimeStatisticsYear_main_Buoc2
	GROUP BY CrimeType_FK, Year_FK
) AS TABLE_A
JOIN 
(
	SELECT CrimeType_FK, SUM([Percent]) AS Tong_Percent
	FROM view_CrimeStatisticsYear_main_Buoc2
	GROUP BY CrimeType_FK
) AS TABLE_B ON TABLE_A.CrimeType_FK = TABLE_B.CrimeType_FK
GO
/****** Object:  View [dbo].[view_CrimeStatisticsYear_main]    Script Date: 30/10/2019 9:26:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_CrimeStatisticsYear_main]
as
SELECT (SELECT B.Id FROM Chicago_DDS.dbo.Year B WHERE B.Year = A.year) AS Year_FK
		,(SELECT B.Id FROM Chicago_DDS.dbo.CrimeType B WHERE B.Name LIKE A.IUCR) AS CrimeType_FK
		,A.[Percent]
FROM view_CrimeStatisticsYear A
GO
/****** Object:  Table [dbo].[Beat]    Script Date: 30/10/2019 9:26:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Beat](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[BeatNum] [int] NULL,
	[Beat] [int] NULL,
	[TheGoem] [varchar](1000) NULL,
	[District_FK] [int] NULL,
	[Sector] [int] NULL,
	[Created] [datetime] NULL,
	[Modified] [datetime] NULL,
	[SourceId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[BeatNum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CommunityArea]    Script Date: 30/10/2019 9:26:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CommunityArea](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AreaNum] [int] NULL,
	[TheGoem] [varchar](1000) NULL,
	[Community] [varchar](255) NULL,
	[AreaNum1] [int] NULL,
	[ShapeLen] [float] NULL,
	[ShapeArea] [float] NULL,
	[HousingCrowed] [float] NULL,
	[BelowPoverty] [float] NULL,
	[Unemployed] [float] NULL,
	[WithoutHighSchool] [float] NULL,
	[Under18Over64] [float] NULL,
	[Income] [int] NULL,
	[Harship] [int] NULL,
	[Created] [datetime] NULL,
	[Modified] [datetime] NULL,
	[SourceId] [int] NULL,
	[SourceID2] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[AreaNum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Coordinate]    Script Date: 30/10/2019 9:26:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Coordinate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[XCoordinate] [float] NULL,
	[YCoordinate] [float] NULL,
	[Latitude] [float] NULL,
	[Longitude] [float] NULL,
	[Created] [datetime] NULL,
	[Modified] [datetime] NULL,
	[SourceId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[District]    Script Date: 30/10/2019 9:26:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[District](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DisNum] [int] NULL,
	[DisLabel] [varchar](5) NULL,
	[TheGoem] [varchar](1000) NULL,
	[Created] [datetime] NULL,
	[Modified] [datetime] NULL,
	[SourceId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[DisNum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FBICode]    Script Date: 30/10/2019 9:26:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FBICode](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FBICode] [varchar](255) NULL,
	[NIBRSCode_FK] [int] NULL,
	[CrimeCategory] [varchar](255) NULL,
	[Created] [datetime] NULL,
	[Modified] [datetime] NULL,
	[SourceId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[FBICode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NIBRS]    Script Date: 30/10/2019 9:26:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NIBRS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[NIBRSCode] [varchar](255) NULL,
	[CrimeName] [varchar](255) NULL,
	[Definition] [varchar](1024) NULL,
	[Created] [datetime] NULL,
	[Modified] [datetime] NULL,
	[SourceId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[NIBRSCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ward]    Script Date: 30/10/2019 9:26:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ward](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[WardId] [int] NULL,
	[TheGoem] [varchar](1000) NULL,
	[ShapeLen] [float] NULL,
	[ShapeArea] [float] NULL,
	[Created] [datetime] NULL,
	[Modified] [datetime] NULL,
	[SourceId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[WardId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Beat]  WITH CHECK ADD  CONSTRAINT [FK_BEAT_DISTRICT] FOREIGN KEY([District_FK])
REFERENCES [dbo].[District] ([ID])
GO
ALTER TABLE [dbo].[Beat] CHECK CONSTRAINT [FK_BEAT_DISTRICT]
GO
ALTER TABLE [dbo].[Crime]  WITH CHECK ADD  CONSTRAINT [FK_CRIME_BEAT] FOREIGN KEY([Beat_FK])
REFERENCES [dbo].[Beat] ([ID])
GO
ALTER TABLE [dbo].[Crime] CHECK CONSTRAINT [FK_CRIME_BEAT]
GO
ALTER TABLE [dbo].[Crime]  WITH CHECK ADD  CONSTRAINT [FK_CRIME_COMMUNITYAREA] FOREIGN KEY([CommunityArea_FK])
REFERENCES [dbo].[CommunityArea] ([ID])
GO
ALTER TABLE [dbo].[Crime] CHECK CONSTRAINT [FK_CRIME_COMMUNITYAREA]
GO
ALTER TABLE [dbo].[Crime]  WITH CHECK ADD  CONSTRAINT [FK_CRIME_COORDINATE] FOREIGN KEY([Coordinate_FK])
REFERENCES [dbo].[Coordinate] ([ID])
GO
ALTER TABLE [dbo].[Crime] CHECK CONSTRAINT [FK_CRIME_COORDINATE]
GO
ALTER TABLE [dbo].[Crime]  WITH CHECK ADD  CONSTRAINT [FK_CRIME_IUCR] FOREIGN KEY([IUCR_FK])
REFERENCES [dbo].[IUCR] ([ID])
GO
ALTER TABLE [dbo].[Crime] CHECK CONSTRAINT [FK_CRIME_IUCR]
GO
ALTER TABLE [dbo].[Crime]  WITH CHECK ADD  CONSTRAINT [FK_CRIME_NIBRSCODE] FOREIGN KEY([NIBRSCode_FK])
REFERENCES [dbo].[NIBRS] ([ID])
GO
ALTER TABLE [dbo].[Crime] CHECK CONSTRAINT [FK_CRIME_NIBRSCODE]
GO
ALTER TABLE [dbo].[Crime]  WITH CHECK ADD  CONSTRAINT [FK_CRIME_WARD] FOREIGN KEY([Ward_FK])
REFERENCES [dbo].[Ward] ([ID])
GO
ALTER TABLE [dbo].[Crime] CHECK CONSTRAINT [FK_CRIME_WARD]
GO
ALTER TABLE [dbo].[FBICode]  WITH CHECK ADD  CONSTRAINT [FK_FBICODE_NIBRSCODE] FOREIGN KEY([NIBRSCode_FK])
REFERENCES [dbo].[NIBRS] ([ID])
GO
ALTER TABLE [dbo].[FBICode] CHECK CONSTRAINT [FK_FBICODE_NIBRSCODE]
GO
