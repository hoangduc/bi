USE [Chicago_Stage]
GO
/****** Object:  Table [dbo].[Beat]    Script Date: 30/10/2019 9:28:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Beat](
	[BeatNum] [int] NOT NULL,
	[Beat] [int] NULL,
	[TheGoem] [varchar](max) NULL,
	[District] [int] NULL,
	[Sector] [int] NULL,
	[Created] [datetime] NULL,
	[Modified] [datetime] NULL,
	[SourceId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[BeatNum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CommunityArea]    Script Date: 30/10/2019 9:28:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CommunityArea](
	[AreaNum] [int] NOT NULL,
	[TheGoem] [varchar](max) NULL,
	[Community] [varchar](255) NULL,
	[AreaNum1] [int] NULL,
	[ShapeLen] [float] NULL,
	[ShapeArea] [float] NULL,
	[Created] [datetime] NULL,
	[Modified] [datetime] NULL,
	[SourceId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[AreaNum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CommunityArea2]    Script Date: 30/10/2019 9:28:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CommunityArea2](
	[AreaNum] [int] NOT NULL,
	[Community] [varchar](255) NULL,
	[HousingCrowed] [float] NULL,
	[BelowPoverty] [float] NULL,
	[Unemployed] [float] NULL,
	[WithoutHighSchool] [float] NULL,
	[Under18Over64] [float] NULL,
	[Income] [int] NULL,
	[Harship] [int] NULL,
	[Created] [datetime] NULL,
	[Modified] [datetime] NULL,
	[SourceId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[AreaNum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Crime]    Script Date: 30/10/2019 9:28:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Crime](
	[ID] [int] NULL,
	[CrimeID] [int] NOT NULL,
	[CaseNumber] [varchar](10) NULL,
	[Occured] [datetime] NULL,
	[Block] [varchar](50) NULL,
	[IUCR] [varchar](5) NULL,
	[PrimaryType] [varchar](255) NULL,
	[Descrition] [varchar](255) NULL,
	[LocationDesc] [varchar](255) NULL,
	[Arrest] [bit] NULL,
	[Domestic] [bit] NULL,
	[Beat] [int] NULL,
	[District] [int] NULL,
	[Ward] [int] NULL,
	[CommunityArea] [int] NULL,
	[FBICode] [varchar](255) NULL,
	[XCoordinate] [float] NULL,
	[YCoordinate] [float] NULL,
	[Years] [int] NULL,
	[Latitude] [float] NULL,
	[Longitude] [float] NULL,
	[Location] [varchar](50) NULL,
	[Created] [datetime] NULL,
	[Modified] [datetime] NULL,
	[SourceId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[CrimeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Crime_Backup]    Script Date: 30/10/2019 9:28:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Crime_Backup](
	[ID] [int] NULL,
	[CrimeID] [int] NOT NULL,
	[CaseNumber] [varchar](10) NULL,
	[Occured] [datetime] NULL,
	[Block] [varchar](50) NULL,
	[IUCR] [varchar](5) NULL,
	[PrimaryType] [varchar](255) NULL,
	[Descrition] [varchar](255) NULL,
	[LocationDesc] [varchar](255) NULL,
	[Arrest] [bit] NULL,
	[Domestic] [bit] NULL,
	[Beat] [int] NULL,
	[District] [int] NULL,
	[Ward] [int] NULL,
	[CommunityArea] [int] NULL,
	[FBICode] [varchar](255) NULL,
	[XCoordinate] [float] NULL,
	[YCoordinate] [float] NULL,
	[Years] [int] NULL,
	[Latitude] [float] NULL,
	[Longitude] [float] NULL,
	[Location] [varchar](50) NULL,
	[Created] [datetime] NULL,
	[Modified] [datetime] NULL,
	[SourceId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[CrimeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[District]    Script Date: 30/10/2019 9:28:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[District](
	[DisNum] [int] NOT NULL,
	[DisLabel] [varchar](5) NULL,
	[TheGoem] [varchar](max) NULL,
	[Created] [datetime] NULL,
	[Modified] [datetime] NULL,
	[SourceId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[DisNum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FBICode]    Script Date: 30/10/2019 9:28:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FBICode](
	[FBICode] [varchar](255) NOT NULL,
	[NIBRSCode] [varchar](255) NULL,
	[CrimeCategory] [varchar](255) NULL,
	[Created] [datetime] NULL,
	[Modified] [datetime] NULL,
	[SourceId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[FBICode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[IUCR]    Script Date: 30/10/2019 9:28:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IUCR](
	[IUCRCode] [char](5) NOT NULL,
	[PrimaryDesc] [varchar](255) NULL,
	[SecondDesc] [varchar](255) NULL,
	[IndexCode] [varchar](50) NULL,
	[Created] [datetime] NULL,
	[Modified] [datetime] NULL,
	[SourceId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[IUCRCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NIBRS]    Script Date: 30/10/2019 9:28:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NIBRS](
	[NIBRSCode] [varchar](255) NOT NULL,
	[CrimeName] [varchar](255) NULL,
	[Definition] [varchar](1024) NULL,
	[Created] [datetime] NULL,
	[Modified] [datetime] NULL,
	[SourceId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[NIBRSCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ward]    Script Date: 30/10/2019 9:28:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ward](
	[WardId] [int] NOT NULL,
	[TheGoem] [varchar](1000) NULL,
	[ShapeLen] [float] NULL,
	[ShapeArea] [float] NULL,
	[Created] [datetime] NULL,
	[Modified] [datetime] NULL,
	[SourceId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[WardId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[Str_UpdateCrime]    Script Date: 30/10/2019 9:28:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[Str_UpdateCrime]
as
If(not exists(select *
 from Crime A
 where exists(select*
			 from Crime_Backup B
			 where A.ID = B.ID)))
Begin
	insert into Crime (ID,CrimeID, CaseNumber, Occured, Block, IUCR, PrimaryType, Descrition, LocationDesc, Arrest, Domestic, Beat, District, Ward, CommunityArea, FBICode, XCoordinate, YCoordinate, Years, Latitude, Longitude, Location, Created,Modified,SourceId)
	select B.ID, B.CrimeID, B.CaseNumber, B.Occured, B.Block, B.IUCR, B.PrimaryType, B.Descrition, B.LocationDesc, B.Arrest, B.Domestic, B.Beat, B.District, B.Ward, B.CommunityArea, B.FBICode, B.XCoordinate, B.YCoordinate, B.Years, B.Latitude, B.Longitude, B.Location, GETDATE(), GETDATE(), 8
	from Crime A inner join Crime_Backup B on A.ID = B.ID
end
else
begin
	Update Crime
set Crime.ID = Crime_Backup.ID,
	Crime.CrimeID = Crime_Backup.CrimeId,
	Crime.CaseNumber = Crime_Backup.CaseNumber,
	Crime.Occured = Crime_Backup.Occured,
	Crime.Block = Crime_Backup.Block,
	Crime.IUCR = Crime_Backup.IUCR,
	Crime.PrimaryType = Crime.PrimaryType,
	Crime.Descrition  = Crime_Backup.Descrition,
	Crime.LocationDesc = Crime_Backup.LocationDesc,
	Crime.Arrest = Crime_Backup.Arrest,
	Crime.Domestic = Crime_Backup.Domestic,
	Crime.Beat = Crime_Backup.Beat,
	Crime.District = Crime_Backup.District,
	Crime.Ward = Crime_Backup.Ward,
	Crime.CommunityArea = Crime_Backup.CommunityArea,
	Crime.FBICode = Crime_Backup.FBICode,
	Crime.XCoordinate = Crime_Backup.XCoordinate,
	Crime.YCoordinate = Crime_Backup.YCoordinate,
	Crime.Years = Crime_Backup.Years,
	Crime.Latitude = Crime_Backup.Latitude,
	Crime.Longitude = Crime_Backup.Longitude,
	Crime.Location = Crime_Backup.Location	
from Crime join Crime_Backup on Crime.ID = Crime_Backup.ID
end
GO
