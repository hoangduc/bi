CREATE TRIGGER ID_IDENTITY_Ward
  ON [dbo].[Ward]
  FOR INSERT, DELETE
  AS
  BEGIN
	DECLARE @MAX INT
	SELECT @MAX = MAX(w.ID) FROM [dbo].[Ward] w
	IF(@MAX IS NULL)
	SET @MAX = 0
	DBCC CHECKIDENT ('[dbo].[Ward]', RESEED, @MAX)
  END

-- Câu lệnh delete cho task delete Ward

# Update
  update Ward
  set TheGoem = ?, ShapeLen =?, ShapeArea = ?, Modified = ?
  where SourceId =? and WardId = ?
# Delete
 delete Chicago_NDS.dbo.Ward 
 where WardId not in (select WardId from Chicago_Stage.dbo.Ward) and SourceId in (select SourceId from Chicago_Stage.dbo.Ward)
# Insert
 insert into Ward(WardId,TheGoem,ShapeLen,ShapeArea,Created,Modified,SourceId) values(?,?,?,?,?,?,?)

-- DISTRICT
# Trigger
	CREATE TRIGGER ID_IDENTITY_District
  ON [dbo].[District]
  FOR INSERT, DELETE
  AS
  BEGIN
	DECLARE @MAX INT
	SELECT @MAX = MAX(d.ID) FROM [dbo].[District] d
	IF(@MAX IS NULL)
	SET @MAX = 0
	DBCC CHECKIDENT ('[dbo].[District]', RESEED, @MAX)
  END
#Insert
 insert into District(DisNum, DisLabel,TheGoem,Created,Modified,SourceId) values(?,?,?,?,?,?)

#Update
  update District
  set DisLabel = ?, TheGoem =?, Modified = ?
  where SourceId =? and DisNum = ?

# Delete
 delete Chicago_NDS.dbo.District
 where DisNum not in (select DisNum from Chicago_Stage.dbo.District) and SourceId in (select SourceId from Chicago_Stage.dbo.District)

###############-- BEAT--###########
# Trigger

# Select
select b.BeatNum, b.Beat, b.TheGoem, (select Id from Chicago_NDS.dbo.District d where d.DisNum = District) as District_FK, b.Sector,b.Created, b.Modified from [dbo].[Beat] b

# Lookup
select BeatNum from Chicago_NDS.dbo.Beat where SourceId = 1

#Insert
 insert into Chicago_NDS.dbo.Beat(BeatNum,Beat,TheGoem,District_FK,Sector,Created,Modified,SourceId) values (?,?,?,?,?,?,?,?)

#Update
 update Chicago_NDS.dbo.Beat
   set  Beat=?, TheGoem = ?, District_FK = ?, Sector = ?, Modified = ?
   where SourceId =? and BeatNum = ?
#Delete
 delete Chicago_NDS.dbo.Beat
 where BeatNum not in (select BeatNum from Chicago_Stage.dbo.Beat) and SourceId in (select SourceId from Chicago_Stage.dbo.Beat) 


-- COORDINATE
# Trigger
CREATE TRIGGER ID_IDENTITY_Coordinate
  ON [dbo].[Coordinate]
  FOR INSERT, DELETE
  AS
  BEGIN
	DECLARE @MAX INT
	SELECT @MAX = MAX(d.ID) FROM [dbo].[Coordinate] d
	IF(@MAX IS NULL)
	SET @MAX = 0
	DBCC CHECKIDENT ('[dbo].[Coordinate]', RESEED, @MAX)
  END
# select
select distinct c.XCoordinate, c.YCoordinate, c.Latitude, c.Longitude from Chicago_Stage.dbo.Crime c
where c.XCoordinate not in (select d.XCoordinate from Chicago_NDS.dbo.Coordinate d ) 
	and c.YCoordinate not in (select d.YCoordinate from Chicago_NDS.dbo.Coordinate d ) 
	and c.Latitude not in (select d.Latitude from Chicago_NDS.dbo.Coordinate d ) 
	and c.Longitude not in (select d.Longitude from Chicago_NDS.dbo.Coordinate d ) 
# Update

# Insert

--#######################---IUCR----##############################
#Trigger
CREATE TRIGGER ID_IDENTITY_IUCR
  ON [dbo].[IUCR]
  FOR INSERT, DELETE
  AS
  BEGIN
	DECLARE @MAX INT
	SELECT @MAX = MAX(d.ID) FROM [dbo].[IUCR] d
	IF(@MAX IS NULL)
	SET @MAX = 0
	DBCC CHECKIDENT ('[dbo].[IUCR]', RESEED, @MAX)
  END
# Select IUCR
 select A.IUCRCode, A.PrimaryDesc,A.SecondDesc,A.IndexCode, A.SourceId 
 from Chicago_Stage.dbo.IUCR A
 where A.IndexCode not in (select B.IndexCode from Chicago_NDS.dbo.IUCR B where A.SourceId = B.SourceId)
# Select Crime
 select distinct (tableA.IUCR), tableA.PrimaryType, tableA.Descrition , tableA.SourceId
 from (select A.IUCR, A.PrimaryType, A.Descrition,A.Created, A.Modified,A.SourceId from Chicago_Stage.dbo.Crime A) tableA
 where tableA.IUCR not in (select B.IndexCode from Chicago_NDS.dbo.IUCR B where tableA.SourceId = B.SourceId)

# Update IUCR
update A
set A.PrimaryDesc = B.PrimaryDesc,
	A.SecondDesc = B.SecondDesc,
	A.Created = B.Created,
	A.Modified = B.Modified
from Chicago_NDS.dbo.IUCR A inner join Chicago_Stage.dbo.IUCR  B on A.IUCRCode = B.IUCRCode and A.SourceId = B.SourceId

# update Crime(Chưa sử dụng)

update nguon
set nguon.IUCR = dich.IUCRCode,
    nguon.PrimaryType = dich.PrimaryDesc,
	nguon.Descrition = dich.SecondDesc
from (select distinct (tableA.IUCR), tableA.PrimaryType, tableA.Descrition , tableA.SourceId
from (select A.IUCR, A.PrimaryType, A.Descrition,A.Created, A.Modified,A.SourceId from Chicago_Stage.dbo.Crime A) tableA
where tableA.IUCR not in (select B.IndexCode from Chicago_NDS.dbo.IUCR B where tableA.SourceId = B.SourceId) ) as nguon 
                  inner join Chicago_NDS.dbo.IUCR dich on nguon.IUCR = dich.IUCRCode and nguon.SourceId = dich.SourceId 

# Delete IUCR
delete Chicago_NDS.dbo.IUCR 
where IUCRCode not in (select dich.IUCRCode from Chicago_Stage.dbo.IUCR dich) and SourceId in (select dich.SourceId from Chicago_Stage.dbo.IUCR dich )

--############----Community Area----###############################
# Trigger
CREATE TRIGGER ID_IDENTITY_ComunityArea
  ON [dbo].[CommunityArea]
  FOR INSERT, DELETE
  AS
  BEGIN
	DECLARE @MAX INT
	SELECT @MAX = MAX(d.ID) FROM [dbo].[CommunityArea] d
	IF(@MAX IS NULL)
	SET @MAX = 0
	DBCC CHECKIDENT ('[dbo].[CommunityArea]', RESEED, @MAX)
  END

# Select Community Area
select A.AreaNum, A.TheGoem, A.Community, A.AreaNum1,A.ShapeLen, A.ShapeArea from Chicago_Stage.dbo.CommunityArea A
# Update Commynity Area2

  update A
  set A.HousingCrowed = B.HousingCrowed,
	A.BelowPoverty = B.BelowPoverty,
	A.Unemployed = B.Unemployed,
	A.WithoutHighSchool = B.WithoutHighSchool,
	A.Under18Over64 = B.Under18Over64,
	A.Income = B.Income,
	A.Harship = B.Harship,
	A.Created = GETDATE(),
	A.Modified = GETDATE(),
	A.SourceId2 = 3
  from Chicago_NDS.dbo.CommunityArea A 
		inner join Chicago_Stage.dbo.CommunityArea2 B on A.AreaNum = B.AreaNum
#Update Commnyity Area 1

update A
  set A.TheGoem = B.TheGoem,
		A.Community = B.Community,
		A.AreaNum1 = B.AreaNum1, 
		A.ShapeLen = B.ShapeLen,
		A.ShapeArea = B.ShapeArea,
		A.Modified = GETDATE(),
		A.SourceId = 2
  from Chicago_NDS.dbo.CommunityArea A 
  join Chicago_Stage.dbo.CommunityArea B 
  on A.AreaNum = B.AreaNum and A.SourceId = B.SourceId

#  Dư liệu TEST
 select*from Chicago_Stage.dbo.CommunityArea
  select*from Chicago_Stage.dbo.CommunityArea2
  select*from Chicago_NDS.dbo.CommunityArea
  select count(*) as dich from Chicago_NDS.dbo.CommunityArea
 select count(*) from Chicago_Stage.dbo.CommunityArea


========NIBRS==================
#Trigger
CREATE TRIGGER ID_IDENTITY_NIBRS
  ON [dbo].[NIBRS]
  FOR INSERT, DELETE
  AS
  BEGIN
	DECLARE @MAX INT
	SELECT @MAX = MAX(d.ID) FROM [dbo].[NIBRS] d
	IF(@MAX IS NULL)
	SET @MAX = 0
	DBCC CHECKIDENT ('[dbo].[NIBRS]', RESEED, @MAX)
  END

# Lookup

select A.NIBRSCode from Chicago_NDS.dbo.NIBRS A where A.SourceId = 5 

#Update
update A
set A.CrimeName = ?,
	A.Definition = ?,
	A.Modified = ?
from Chicago_NDS.dbo.NIBRS A 
where A.SourceId = ?, A.NIBRSCode = ?


# Delete

delete Chicago_NDS.dbo.NIBRS 
where NIBRSCode not in (select NIBRSCode from Chicago_Stage.dbo.NIBRS) and SourceId in (select SourceId from Chicago_Stage.dbo.NIBRS)



==========FBICODE====================#######################

#Trigger

CREATE TRIGGER ID_IDENTITY_FBICode
  ON [dbo].[FBICode]
  FOR INSERT, DELETE
  AS
  BEGIN
	DECLARE @MAX INT
	SELECT @MAX = MAX(d.ID) FROM [dbo].[FBICode] d
	IF(@MAX IS NULL)
	SET @MAX = 0
	DBCC CHECKIDENT ('[dbo].[FBICode]', RESEED, @MAX)
  END
# Select(Nguon)

select A.FBICode,(select B.ID from Chicago_NDS.dbo.NIBRS B where A.NIBRSCode like B.NIBRSCode) as NIBRSCode_FK, A.CrimeCategory
from Chicago_Stage.dbo.FBICode A

# Detete

DELETE FROM Chicago_NDS.dbo.FBICode 
WHERE FBICode NOT IN (SELECT FBICode FROM Chicago_Stage.dbo.FBICode) AND FBICode.SourceId IN (SELECT SourceId FROM Chicago_Stage.dbo.FBICode) 


===========CRIME=====================############################
#SELECT

SELECT A.ID AS [Index], A.CrimeID
		, A.CaseNumber, CONVERT(DATE
		, A.Occured) AS OccuredDate
		, CONVERT(TIME, A.Occured) AS OccuredTime
		, A.Block 
		, (SELECT B.ID FROM Chicago_NDS.dbo.IUCR B WHERE A.IUCR LIKE B.IUCRCode ) AS IUCR_FK
		, A.LocationDesc, A.Arrest, A.Domestic
		, (SELECT beat.ID FROM Chicago_NDS.dbo.Beat beat WHERE A.Beat = beat.BeatNum) AS Beat_FK
		, (SELECT ward.ID FROM Chicago_NDS.dbo.Ward ward WHERE A.Ward = ward.WardId) AS ward_FK
		, (SELECT com.ID FROM Chicago_NDS.dbo.CommunityArea com WHERE A.CommunityArea = com.AreaNum ) AS CommunityArea_FK
		, (SELECT ni.ID FROM Chicago_NDS.dbo.NIBRS ni WHERE A.FBICode LIKE ni.NIBRSCode ) AS NIBRSCode_FK
		, (SELECT coor.ID FROM Chicago_NDS.dbo.Coordinate coor WHERE A.XCoordinate = coor.XCoordinate AND coor.YCoordinate = A.YCoordinate AND coor.Latitude = A.Latitude AND coor.Longitude = A.Longitude) AS Coordinate_FK
FROM Chicago_Stage.dbo.Crime A 

#UPDATE
UPDATE Crime
SET	 CaseNumber = ?
	,OccuredDate = ?
	,OccuredTime = ?
	,Block = ?
	,LocationDesc = ?
	,Arrest = ?
	,Domestic =?
	,Beat_FK=?
	,Ward_FK =?
	,CommunityArea_FK =?
	,NIBRSCode_FK =?
	,Coordinate_FK =?
	,Modified = ?
	, IUCR_FK = ? 
WHERE SourceId = ? AND CrimeID = ?

#DELETE
 DELETE dbo.Crime
 WHERE CrimeID NOT IN (SELECT CrimeID FROM Chicago_Stage.dbo.Crime) AND Crime.SourceId IN (SELECT SourceId FROM Chicago_Stage.dbo.Crime)


# TEST KIỂM TRA SỐ DÒNG

SELECT COUNT(*) AS Beat FROM Chicago_Stage.dbo.Beat
SELECT COUNT(*) AS Com FROM Chicago_Stage.dbo.CommunityArea
SELECT COUNT(*) AS com2 FROM Chicago_Stage.dbo.CommunityArea2
SELECT COUNT(*) AS crime FROM Chicago_Stage.dbo.Crime
SELECT COUNT(*) AS Crime_backup FROM Chicago_Stage.dbo.Crime_Backup
SELECT COUNT(*) AS district FROM Chicago_Stage.dbo.District
SELECT COUNT(*) AS FBICode FROM Chicago_Stage.dbo.FBICode
SELECT COUNT(*) AS IUCR FROM Chicago_Stage.dbo.IUCR
SELECT COUNT(*) AS NIBRS FROM Chicago_Stage.dbo.NIBRS
SELECT COUNT(*) AS Ward FROM Chicago_Stage.dbo.Ward 